﻿---

## About

This is a sample code on how to implement TCP/IP Client and Server using TF6310.

---

## Pre-requisite

Download and install TF6310 TC3 TCP/IP Function from Beckhoff website.

Download serial terminal software for testing.
For example: [Hercules](https://www.hw-group.com/software/hercules-setup-utility)

---

## How to use

For example, TCP_Client_EchoString:

Open the serial terminal. Run as a TCP Server.


---

## Help

Contact support@beckhoff.com.sg